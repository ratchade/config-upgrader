package common

import (
	"strings"
)

// This file contains all the upgrades related to the machine TOML table

// getOffpeakFields returns the list of OffpeakField to upgrade
func getOffpeakFields() [4]string {
	return [4]string{"OffPeakIdleCount", "OffPeakIdleTime", "OffPeakPeriods", "OffPeakTimezone"}
}

type DockerMachine struct {
}

// Upgrade upgrades the machine TOML table
// In our case only the Offpeak fields must be upgraded
// However we could easily add other upgrade section as the configuration file evolved
func (dm DockerMachine) Upgrade(i interface{}) interface{} {
	return dm.upgradeOffPeak(i)
}

// upgradeOffpeak is in charge of the offpeak field conversion into a autoscaling table
func (dm DockerMachine) upgradeOffPeak(i interface{}) interface{} {
	m, ok := i.(map[string]interface{})

	if !ok {
		return m
	}

	offpeak := false
	offpeakMap := make(map[string]interface{})

	a, has := m[Autoscaling]

	if !has {
		a = make([]map[string]interface{}, 0)
	}

	for _, f := range getOffpeakFields() {
		_, found := m[f]
		if !found {
			continue
		}

		offpeak = true
		offpeakMap[strings.ReplaceAll(f, "OffPeak", "")] = m[f]
		delete(m, f)
	}

	if offpeak {
		// At the moment the upgrade autoscaling table is added at the end of the existing ones if any
		a = append(a.([]map[string]interface{}), offpeakMap)
		m[Autoscaling] = a
	}

	return m
}
