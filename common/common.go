package common

import (
	"bufio"
	"bytes"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"

	"github.com/BurntSushi/toml"
)

const (
	Machine     = "machine"
	Autoscaling = "autoscaling"
)

type ConfigUpgrader struct {
}

// LoadConfig is used to load the given toml configuration thanks to BurntSushi Library
// This function is basically a copy of the one available in gitlab-runner project
func (cu ConfigUpgrader) LoadConfig(configFile string) (*interface{}, error) {
	var c interface{}
	_, err := os.Stat(configFile)

	// permission denied is soft error
	if os.IsNotExist(err) {
		return nil, errors.New("config file cannot be found")
	} else if err != nil {
		return nil, err
	}

	if _, err = toml.DecodeFile(configFile, &c); err != nil {
		return nil, err
	}

	return &c, nil
}

// SaveConfig is used to save the given data into a toml configuration thanks to BurntSushi Library
// This function is basically a copy of the one available in gitlab-runner project
// The only modification is the data to save is passed as parameter
func (cu ConfigUpgrader) SaveConfig(c interface{}, configFile string) error {
	var newConfig bytes.Buffer

	newBuffer := bufio.NewWriter(&newConfig)

	if err := toml.NewEncoder(newBuffer).Encode(&c); err != nil {
		return err
	}

	if err := newBuffer.Flush(); err != nil {
		return err
	}

	// create directory to store configuration
	err := os.MkdirAll(filepath.Dir(configFile), 0700)
	if err != nil {
		return err
	}

	// write config file
	if err := ioutil.WriteFile(configFile, newConfig.Bytes(), 0600); err != nil {
		return err
	}

	return nil
}

// Upgrader is an interface defining the function used to upgrade a section of the configuration
type Upgrader interface {
	Upgrade(i interface{}) interface{}
}

// List the headers of the upgradable table and the upgrade function associated
var upgrades map[string]Upgrader = map[string]Upgrader{
	Machine: DockerMachine{},
}

// UpgradeCfg recursively "navigate" the interface returned after loading of the configuration
// It is used to identify the two following structure :
// * Table which are map[string]interface{}
// * Array of tables which are []map[string]interface{}
// For our runner configuration, []string/[]int/[]... are used mainly as value for key thus ignored
func UpgradeCfg(i *interface{}) {
	switch reflect.TypeOf(*i).Kind() {
	case reflect.Map:
		m, ok := (*i).(map[string]interface{})
		if ok {
			process(&m)
		}
	case reflect.Slice:
		m, ok := (*i).([]map[string]interface{})
		if ok {
			for _, val := range m {
				process(&val)
			}
		}
	}
}

// process processes the given TOML table and upgrades it if needed
func process(m *map[string]interface{}) {
	for key, val := range *m {
		upgrade, found := upgrades[key]
		switch {
		case found:
			(*m)[key] = upgrade.Upgrade(val)
		default:
			UpgradeCfg(&val)
		}
	}
}
