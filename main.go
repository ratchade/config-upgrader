package main

import (
	"flag"
	"fmt"
	"os"

	"config-upgrader/common"
)

var cu common.ConfigUpgrader

func main() {
	src := flag.String("src", "", "Path to the config file to upgrade")
	dest := flag.String("dest", "", "Path to save the upgraded config file")

	flag.Parse()

	if *src == "" {
		fmt.Println("No source given")
		os.Exit(1)
	}

	if *dest == "" {
		*dest = *src
	}

	data, err := cu.LoadConfig(*src)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	common.UpgradeCfg(data)
	cu.SaveConfig(*data, *dest)
}
