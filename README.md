# config-upgrader

Command as follow :

```sh
go build -o out/config-upgrader
./out/config-upgrader --src=PATH_TO_THE_CONFIG_TOML_TO_UPGRADE --dest=PATH_FOR_UPGRADED_CONFIG_TOML_FILE
```

If `dest` is missing, the `src` is used as destination
